import { useSpring, animated } from "react-spring";
import styled from "@emotion/styled";

const Animated = styled(animated.div)`
  position: absolute;
  border-radius: 5px;
  background-size: cover;
  background-position: center center;
  background-repeat: no-repeat;
  will-change: transform;

  width: 25ch;
  height: 25ch;
  max-width: 100ch;
  max-height: 100ch;
  background-image: ${({ url }: { url: string }) => `url(${url})`};
`;

const trans: any = ({ divider = 1, offset = { x: 0, y: 0 } }) => (
  x: number,
  y: number
) => `translate3d(${x / divider + offset.x}px,${y / divider + offset.y}px,0)`;

const AnimatedCard = ({ url, xy, divider, offset, width, height }: any) => {
  return (
    <Animated
      url={url}
      style={{
        width,
        height,
        transform: xy.interpolate(trans({ divider, offset, width })),
      }}
    />
  );
};

const calc = (x: number, y: number) => [
  x - window.innerWidth / 2,
  y - window.innerHeight / 2,
];

export default function Card() {
  const [props, set] = useSpring(() => ({
    xy: [0, 0],
    config: { mass: 10, tension: 550, friction: 140 },
  }));
  return (
    <div
      className="container"
      onMouseMove={({ clientX: x, clientY: y }) => set({ xy: calc(x, y) })}
    >
      <AnimatedCard
        url="https://image.flaticon.com/icons/svg/119/119596.svg"
        xy={props.xy}
        divider={10}
        width="45vw"
        height="45vw"
      />
      <AnimatedCard
        url="https://image.flaticon.com/icons/svg/789/789395.svg"
        xy={props.xy}
        divider={8}
        offset={{ x: 35, y: -230 }}
      />
      <AnimatedCard
        url="https://image.flaticon.com/icons/svg/414/414927.svg"
        xy={props.xy}
        divider={6}
        offset={{ x: -250, y: -200 }}
      />
      <AnimatedCard
        url="https://image.flaticon.com/icons/svg/789/789392.svg"
        xy={props.xy}
        divider={3.5}
      />
    </div>
  );
}
